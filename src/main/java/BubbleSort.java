import java.util.Arrays;
/**
 * 冒泡排序的规则
 * 1、从第一个数开始，将这个数 与它相邻的数比较，如果这个数大于相邻的数
 * 则两个数交换位置
 * 2、依次从第二个数开始比较，与相邻的数比较
 * 3、以此类推 到倒数第二数截至
 * 4、重复以上步骤
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48};
        //用于临时交换的变量
        int temp = 0;
        for (int j = 0; j < array.length - 1; j++) {
            for (int i = 0; i < array.length - j - 1; i++) {
                if (array[i] > array[i + 1]) {  //相邻的数比较
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
            System.out.println("比较"+(j+1)+"轮之后：" + Arrays.toString(array));
        }
    }
}
