import java.util.Arrays;
/**
 * 插入排序
 * 1、从第一个元素开始，假设第一个元素是已排好序的
 * 2、从下一个元素开始，依次比较它前面的所有元素(从后向前扫描)
 * 3、 如果这个元素 小于它前面的元素 则两两交换 ，
 * 如果这个元素 大于它前面的元素，则不交换
 * 4、依次重复2,3步骤 ，直到将所有数 比较完成
 */
public class InsertSort {
    public static void main(String[] args) {
        int [] array = {3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48};
        for(int i = 1;i < array.length; i++){
            int key = array[i];
            int j = i - 1;
            while((j >= 0) && (key < array[j])){
                array[j + 1]=array[j];
                j--;
            }
            array[j + 1]=key;
            System.out.println("第" + i +"次完成后："+ Arrays.toString(array));
        }
        System.out.println("最后一次完成后的结果："+Arrays.toString(array));
    }
}
