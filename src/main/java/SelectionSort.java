import java.util.Arrays;
/**
 * 选择排序： 从一堆数中选择一个最小数 放在第一个位置，再从一堆数中选择一个
 * 最小数放在第二个位置， 依次 将一堆数的最小数按顺序排放。
 * 步骤：
 * 1、假设第一个数是最小数，需要定义最小数的下标minIndex=0
 * 将这个数与后面的每一个数比较，找到最小数的下标即可
 * 2、将第一个数与最小数的下标交换 ，得出最小数在第一位。
 * 3、依次类推， 将已比较的数 忽略，继续从生下的元素中找足最小数，放入已比较的数的下一位
 * 直到整个数列比较结束
 */
public class SelectionSort {
    public static void main(String[] args) {
        int [] array = {3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48};
        for(int j = 0;j <= array.length-1;j++){
            int minIndex = j;   // 假设第一个数是最小数
            for (int i = 1+j; i < array.length; i++) {  // 为什么i =j+1 因为初始值要略过已比较的下标
                if (array[minIndex] > array[i]) {
                    minIndex = i;
                    int temp = 0;
                    temp = array[j];        // 将这个最小数放在 第一位
                    array[j] = array[minIndex];
                    array[minIndex] = temp;
                }
            }
            System.out.println("第" + (j + 1) +"次完成后：" + Arrays.toString(array));
        }
        System.out.println("最后的排序：" + Arrays.toString(array));
    }
}


