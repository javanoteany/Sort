public class ShellSort {
    public static void main(String[] args) {
        int array[] = {1,2,4,3,9,7,8,6};
        int h = 0;
        int length = array.length;
        while( h <= length ){ //计算首次步长
            h = 3 * h + 1;
        }
        while( h >= 1 ){
            for( int i = h;i < length;i++ ){
                int j = i - h; //左边的一个元素
                int get = array[i]; //当前元素
                while( j >= 0 && array[j] > get ){ //左边的比当前大，则左边的往右边挪动
                    array[j+h] = array[j];
                    j = j - h;
                }
                array[j + h] = get; //挪动完了之后把当前元素放进去
            }
            h = ( h - 1 ) / 3;
        }
        for( int i = 0 ; i < array.length ; i++ ){
            System.out.print(array[i]+" ");
        }
    }
}
